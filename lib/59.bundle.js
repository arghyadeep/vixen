(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[59],{

/***/ "./node_modules/@theia/core/lib/browser/widgets/alert-message.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@theia/core/lib/browser/widgets/alert-message.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2018 Ericsson and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlertMessage = void 0;
var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");
var AlertMessageIcon = {
    INFO: 'fa fa-info-circle',
    SUCCESS: 'fa fa-check-circle',
    WARNING: 'fa fa-exclamation-circle',
    ERROR: 'fa fa-times-circle'
};
var AlertMessage = /** @class */ (function (_super) {
    __extends(AlertMessage, _super);
    function AlertMessage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AlertMessage.prototype.render = function () {
        return React.createElement("div", { className: 'theia-alert-message-container' },
            React.createElement("div", { className: "theia-" + this.props.type.toLowerCase() + "-alert" },
                React.createElement("div", { className: 'theia-message-header' },
                    React.createElement("i", { className: AlertMessageIcon[this.props.type] }),
                    "\u00A0",
                    this.props.header),
                React.createElement("div", { className: 'theia-message-content' }, this.props.children)));
    };
    return AlertMessage;
}(React.Component));
exports.AlertMessage = AlertMessage;


/***/ }),

/***/ "./node_modules/@theia/navigator/lib/browser/index.js":
/*!************************************************************!*\
  !*** ./node_modules/@theia/navigator/lib/browser/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2018 TypeFox and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(__webpack_require__(/*! ./navigator-model */ "./node_modules/@theia/navigator/lib/browser/navigator-model.js"), exports);
__exportStar(__webpack_require__(/*! ./navigator-widget */ "./node_modules/@theia/navigator/lib/browser/navigator-widget.js"), exports);
__exportStar(__webpack_require__(/*! ./navigator-decorator-service */ "./node_modules/@theia/navigator/lib/browser/navigator-decorator-service.js"), exports);


/***/ }),

/***/ "./node_modules/@theia/navigator/lib/browser/navigator-decorator-service.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@theia/navigator/lib/browser/navigator-decorator-service.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2018 TypeFox and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NavigatorDecoratorService = exports.NavigatorTreeDecorator = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var contribution_provider_1 = __webpack_require__(/*! @theia/core/lib/common/contribution-provider */ "./node_modules/@theia/core/lib/common/contribution-provider.js");
var tree_decorator_1 = __webpack_require__(/*! @theia/core/lib/browser/tree/tree-decorator */ "./node_modules/@theia/core/lib/browser/tree/tree-decorator.js");
/**
 * Symbol for all decorators that would like to contribute into the navigator.
 */
exports.NavigatorTreeDecorator = Symbol('NavigatorTreeDecorator');
/**
 * Decorator service for the navigator.
 */
var NavigatorDecoratorService = /** @class */ (function (_super) {
    __extends(NavigatorDecoratorService, _super);
    function NavigatorDecoratorService(contributions) {
        var _this = _super.call(this, contributions.getContributions()) || this;
        _this.contributions = contributions;
        return _this;
    }
    NavigatorDecoratorService = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject(contribution_provider_1.ContributionProvider)), __param(0, inversify_1.named(exports.NavigatorTreeDecorator)),
        __metadata("design:paramtypes", [Object])
    ], NavigatorDecoratorService);
    return NavigatorDecoratorService;
}(tree_decorator_1.AbstractTreeDecoratorService));
exports.NavigatorDecoratorService = NavigatorDecoratorService;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-frontend-module.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-frontend-module.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTimelineTreeContainer = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var widget_manager_1 = __webpack_require__(/*! @theia/core/lib/browser/widget-manager */ "./node_modules/@theia/core/lib/browser/widget-manager.js");
var timeline_service_1 = __webpack_require__(/*! ./timeline-service */ "./node_modules/@theia/timeline/lib/browser/timeline-service.js");
var timeline_widget_1 = __webpack_require__(/*! ./timeline-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-widget.js");
var timeline_tree_widget_1 = __webpack_require__(/*! ./timeline-tree-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-tree-widget.js");
var browser_1 = __webpack_require__(/*! @theia/core/lib/browser */ "./node_modules/@theia/core/lib/browser/index.js");
var timeline_tree_model_1 = __webpack_require__(/*! ./timeline-tree-model */ "./node_modules/@theia/timeline/lib/browser/timeline-tree-model.js");
var timeline_empty_widget_1 = __webpack_require__(/*! ./timeline-empty-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-empty-widget.js");
var timeline_context_key_service_1 = __webpack_require__(/*! ./timeline-context-key-service */ "./node_modules/@theia/timeline/lib/browser/timeline-context-key-service.js");
var timeline_contribution_1 = __webpack_require__(/*! ./timeline-contribution */ "./node_modules/@theia/timeline/lib/browser/timeline-contribution.js");
__webpack_require__(/*! ../../src/browser/style/index.css */ "./node_modules/@theia/timeline/src/browser/style/index.css");
var common_1 = __webpack_require__(/*! @theia/core/lib/common */ "./node_modules/@theia/core/lib/common/index.js");
var tab_bar_toolbar_1 = __webpack_require__(/*! @theia/core/lib/browser/shell/tab-bar-toolbar */ "./node_modules/@theia/core/lib/browser/shell/tab-bar-toolbar.js");
exports.default = new inversify_1.ContainerModule(function (bind) {
    bind(timeline_contribution_1.TimelineContribution).toSelf().inSingletonScope();
    bind(common_1.CommandContribution).toService(timeline_contribution_1.TimelineContribution);
    bind(tab_bar_toolbar_1.TabBarToolbarContribution).toService(timeline_contribution_1.TimelineContribution);
    bind(timeline_context_key_service_1.TimelineContextKeyService).toSelf().inSingletonScope();
    bind(timeline_service_1.TimelineService).toSelf().inSingletonScope();
    bind(timeline_widget_1.TimelineWidget).toSelf();
    bind(widget_manager_1.WidgetFactory).toDynamicValue(function (_a) {
        var container = _a.container;
        return ({
            id: timeline_widget_1.TimelineWidget.ID,
            createWidget: function () { return container.get(timeline_widget_1.TimelineWidget); }
        });
    }).inSingletonScope();
    bind(timeline_tree_widget_1.TimelineTreeWidget).toDynamicValue(function (ctx) {
        var child = createTimelineTreeContainer(ctx.container);
        return child.get(timeline_tree_widget_1.TimelineTreeWidget);
    });
    bind(widget_manager_1.WidgetFactory).toDynamicValue(function (_a) {
        var container = _a.container;
        return ({
            id: timeline_tree_widget_1.TimelineTreeWidget.ID,
            createWidget: function () { return container.get(timeline_tree_widget_1.TimelineTreeWidget); }
        });
    }).inSingletonScope();
    bind(timeline_empty_widget_1.TimelineEmptyWidget).toSelf();
    bind(widget_manager_1.WidgetFactory).toDynamicValue(function (_a) {
        var container = _a.container;
        return ({
            id: timeline_empty_widget_1.TimelineEmptyWidget.ID,
            createWidget: function () { return container.get(timeline_empty_widget_1.TimelineEmptyWidget); }
        });
    }).inSingletonScope();
});
function createTimelineTreeContainer(parent) {
    var child = browser_1.createTreeContainer(parent, {
        virtualized: true,
        search: true
    });
    child.unbind(browser_1.TreeWidget);
    child.bind(timeline_tree_widget_1.TimelineTreeWidget).toSelf();
    child.unbind(browser_1.TreeModelImpl);
    child.bind(timeline_tree_model_1.TimelineTreeModel).toSelf();
    child.rebind(browser_1.TreeModel).toService(timeline_tree_model_1.TimelineTreeModel);
    return child;
}
exports.createTimelineTreeContainer = createTimelineTreeContainer;


/***/ }),

/***/ "./node_modules/@theia/timeline/src/browser/style/index.css":
/*!******************************************************************!*\
  !*** ./node_modules/@theia/timeline/src/browser/style/index.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../css-loader!./index.css */ "./node_modules/css-loader/index.js!./node_modules/@theia/timeline/src/browser/style/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/@theia/variable-resolver/lib/browser/index.js":
/*!********************************************************************!*\
  !*** ./node_modules/@theia/variable-resolver/lib/browser/index.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2018 Red Hat, Inc. and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(__webpack_require__(/*! ./variable */ "./node_modules/@theia/variable-resolver/lib/browser/variable.js"), exports);
__exportStar(__webpack_require__(/*! ./variable-quick-open-service */ "./node_modules/@theia/variable-resolver/lib/browser/variable-quick-open-service.js"), exports);
__exportStar(__webpack_require__(/*! ./variable-resolver-service */ "./node_modules/@theia/variable-resolver/lib/browser/variable-resolver-service.js"), exports);


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/@theia/timeline/src/browser/style/index.css":
/*!********************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/@theia/timeline/src/browser/style/index.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/********************************************************************************\n * Copyright (C) 2020 RedHat and others.\n *\n * This program and the accompanying materials are made available under the\n * terms of the Eclipse Public License v. 2.0 which is available at\n * http://www.eclipse.org/legal/epl-2.0.\n *\n * This Source Code may also be made available under the following Secondary\n * Licenses when the conditions for such availability set forth in the Eclipse\n * Public License v. 2.0 are satisfied: GNU General Public License, version 2\n * with the GNU Classpath Exception which is available at\n * https://www.gnu.org/software/classpath/license.html.\n *\n * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0\n ********************************************************************************/\n\n\n.theia-timeline {\n    box-sizing: border-box;\n    height: 100%;\n    display: flex;\n    flex-direction: column;\n}\n\n.theia-timeline .timeline-outer-container {\n    overflow-y: auto;\n    width: 100%;\n    flex-grow: 1;\n}\n\n\n.theia-timeline .timeline-item {\n    font-size: var(--theia-ui-font-size1);\n    display: flex;\n    overflow: hidden;\n    align-items: center;\n    justify-content: space-between;\n    height: var(--theia-content-line-height);\n    line-height: var(--theia-content-line-height);\n    padding: 0px calc(var(--theia-ui-padding)/2);\n}\n\n.theia-timeline .timeline-item:hover {\n    cursor: pointer;\n}\n\n.theia-timeline .timeline-item .label {\n    font-size: var(--theia-ui-font-size0);\n    margin-left: var(--theia-ui-padding);\n    opacity: .7;\n}\n", ""]);

// exports


/***/ })

}]);
//# sourceMappingURL=59.bundle.js.map