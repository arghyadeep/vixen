# Vixen - IDE

## Description

A lightweight IDE for the cloud based on VSCode design and made out of Theia.

<br>

## Installations Required 

- git
- npm
- nodejs 12.0 or higher
- make
- gcc
- pkg-config
- build-essential
- libx11-dev 
- libxkbfile-dev 
- yarn
- caddy

<br>

## Commands 

Replace your domain or Reverse DNS url in the Caddyfile and run the following commands.

- `yarn`
- `yarn theia build`
- `pm2 start yarn --interpreter bash --name vixen-ide -- start`



To change the default login credentials:
- Generate a hashed password using `caddy hash-password`.
- Replace the hashed string in your Caddyfile.
- Replace the username as per your wish.


<br>

## Default Login

- Username: admin
- Password: arghyadeep

